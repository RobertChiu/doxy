#!/bin/bash

set -x

echo "Sending results to qut server"

TIME_NOW=$(</home/timestamp.txt)
LAST_COMMIT_USER=$(git log -1 --pretty=format:'%an')
LAST_COMMIT_ID=$(git log --pretty=format:'%h' -n 1)

LAST_COMMIT_BRANCH_ID=$(git name-rev "$LAST_COMMIT_ID")	
LAST_COMMIT_BRANCH=$(echo "$LAST_COMMIT_BRANCH_ID" | awk '{print $2}' | awk -F/ '{print $1}')

BUILD_PATH=/home/doxyLogs/$LAST_COMMIT_BRANCH/$LAST_COMMIT_ID:$LAST_COMMIT_USER-$TIME_NOW
BUILD_PATH=$(echo "$BUILD_PATH" | tr -d '[:space:]')

echo "Setting up the qut environment"

QUT_HOME=/home/qut
INITIALIZE_LOG_FILE=$BUILD_PATH/Tests/testlogs.txt
INITIALIZE_PROJECT=Doxy
INITIALIZE_BRANCH=$LAST_COMMIT_BRANCH
INITIALIZE_PROJECT_TYPE=CSHARP

echo "Starting up the maven commands on $TIME_NOW"

echo "mvn clean install $QUT_HOME/qut.dto/"
cd $QUT_HOME/qut.dto/
mvn clean install

echo "mvn clean install $QUT_HOME/qut.client/"
cd $QUT_HOME/qut.client/
mvn clean install

echo "mvn clean install $QUT_HOME/initialize-test-run-maven-plugin/"
cd $QUT_HOME/initialize-test-run-maven-plugin/
mvn clean install

echo "mvn clean install $QUT_HOME/qut.test.unity/"
cd $QUT_HOME/qut.test.unity/
mvn clean install

echo "Running the initialize plugin with initialize goal and properties: logFile=$INITIALIZE_LOG_FILE project=$INITIALIZE_PROJECT  branch=$INITIALIZE_BRANCH runDate=$TIME_NOW projectType=$INITIALIZE_PROJECT_TYPE"
mvn com.scotchboard.plugin:initialize-test-run-maven-plugin:0.0.1-SNAPSHOT:initialize -DlogFile="$INITIALIZE_LOG_FILE" -Dproject=$INITIALIZE_PROJECT  -Dbranch="$INITIALIZE_BRANCH" -DrunDate="$TIME_NOW" -DprojectType=$INITIALIZE_PROJECT_TYPE -e -X

echo "Finished the maven commands"

sudo rm /home/timestamp.txt
cd $QUT_HOME