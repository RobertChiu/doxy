#!/bin/bash

set -e
set -x

echo "Building for $BUILD_TARGET"

TIME_NOW=$(</home/timestamp.txt)
LAST_COMMIT_USER=$(git log -1 --pretty=format:'%an')
LAST_COMMIT_ID=$(git log --pretty=format:'%h' -n 1)

LAST_COMMIT_BRANCH_ID=$(git name-rev "$LAST_COMMIT_ID")	
LAST_COMMIT_BRANCH=$(echo "$LAST_COMMIT_BRANCH_ID" | awk '{print $2}' | awk -F/ '{print $1}')

BUILD_PATH=/home/doxyLogs/$LAST_COMMIT_BRANCH/$LAST_COMMIT_ID:$LAST_COMMIT_USER-$TIME_NOW/Build/$BUILD_TARGET
BUILD_PATH=$(echo "$BUILD_PATH" | tr -d '[:space:]')

sudo mkdir -p $BUILD_PATH

sudo ${UNITY_EXECUTABLE} \
  -projectPath $UNITY_DIR \
  -quit \
  -batchmode \
  -nographics \
  -logFile $BUILD_PATH/buildLog.txt

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

sudo ls -la $BUILD_PATH
[ -n "$(sudo ls -A $BUILD_PATH)" ] # fail job if build folder is empty
