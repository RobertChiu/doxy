#!/bin/bash

set -x

echo "Testing for $TEST_PLATFORM"

TIME_NOW=$(</home/timestamp.txt)
LAST_COMMIT_USER=$(git log -1 --pretty=format:'%an')
LAST_COMMIT_ID=$(git log --pretty=format:'%h' -n 1)

LAST_COMMIT_BRANCH_ID=$(git name-rev "$LAST_COMMIT_ID")	
LAST_COMMIT_BRANCH=$(echo "$LAST_COMMIT_BRANCH_ID" | awk '{print $2}' | awk -F/ '{print $1}')

BUILD_PATH=/home/doxyLogs/$LAST_COMMIT_BRANCH/$LAST_COMMIT_ID:$LAST_COMMIT_USER-$TIME_NOW
BUILD_PATH=$(echo "$BUILD_PATH" | tr -d '[:space:]')

sudo ${UNITY_EXECUTABLE} \
  -projectPath $UNITY_DIR \
  -runTests \
  -testPlatform $TEST_PLATFORM \
  -testResults $BUILD_PATH/Tests/$TEST_PLATFORM-results.xml \
  -logFile $BUILD_PATH/Build/$TEST_PLATFORM-buildLog.txt \
  -batchmode \
  -nographics \

UNITY_EXIT_CODE=$?

if [ $UNITY_EXIT_CODE -eq 0 ]; then
  echo "Run succeeded, no failures occurred";
elif [ $UNITY_EXIT_CODE -eq 2 ]; then
  echo "Run succeeded, some tests failed";
elif [ $UNITY_EXIT_CODE -eq 3 ]; then
  echo "Run failure (other failure)";
else
  echo "Unexpected exit code $UNITY_EXIT_CODE";
fi

# Check the generated xml if the tests were successful or not
sudo cat $BUILD_PATH/Tests/$TEST_PLATFORM-results.xml | grep test-run | grep Passed

# Move the test log file where all the logs for the current build are
sudo mv $GIT_CLONE_PATH/Assets/StreamingAssets/Logs/testlogs.txt $BUILD_PATH/Tests