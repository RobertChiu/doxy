#!/usr/bin/env sh

set -x

export UNITY_EXECUTABLE="/root/Unity2020.1.10f1/Editor/Unity"
export UNITY_DIR="/home/gitlab-runner/builds"

BUILD_TARGET=StandaloneWindows64 ./ci/build.sh
