#!/usr/bin/env sh

set -x

export UNITY_EXECUTABLE="/root/Unity2020.1.10f1/Editor/Unity"
export UNITY_DIR="/home/gitlab-runner/builds"

TEST_PLATFORM=editmode ./ci/test.sh
#TEST_PLATFORM=playmode ./ci/test.sh
